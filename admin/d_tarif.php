<?php  
include 'navbar.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>

<div class="row-fluid sortable">    
        <div class="box span12">
          <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Data Tarif Listrik</h2>
            <div class="box-icon">
              <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
              <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
              <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
          </div>
          <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                <tr>
                  <th>Kode Tarif</th>
                  <th>Daya</th>
                  <th>Tarif</th>
                  <th>Aksi</th>
                </tr>
              </thead>   
              <tbody>
                <?php 
                $pilih = mysqli_query ($koneksi,"SELECT * FROM tarif");
                while($data=mysqli_fetch_array($pilih)){
                ?>
                <tr>
                  <td><?php echo $data['id_tarif']; ?></td>
                  <td><?php echo $data['daya']; ?></td>
                  <td><?php echo $data['tarifperkwh']; ?></td>
                  <td>
                    <a href="e_tarif.php?id=<?php echo $data['id_tarif']; ?>&aksi=edit"> <button class="btn btn-primary" aria-hidden="true">Edit</button></a>
                    <a href="hapust.php?id=<?php echo $data['id_tarif']; ?>&aksi=hapus"> <button class="btn btn-danger" aria-hidden="true">Hapus</button></a>
                      </td>
                    </tr>
                  <?php  
                    }
                  ?>
              </tbody>
            </table> 
            <a class='btn btn-dark' href='tambah_t.php'>Tambah</a>
            <button class="btn btn-success" onClick="print_d()">Print Document</button>           
          </div>
        </div><!--/span-->
      
      </div><!--/row-->

<script>
  function print_d(){
   window.open("printt.php","_blank");
}
</script>
<?php  
include 'footer.php';
?>