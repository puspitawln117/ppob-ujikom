

CREATE TABLE `admin` (
  `id_admin` char(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `email` varchar(25) NOT NULL,
  `id_level` int(25) NOT NULL,
  PRIMARY KEY (`id_admin`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("20190403001","admin","21232f297a57a5a743894a0e4a801fc3","Puspita Wulandari","mewaaa@gmail.com","1");





CREATE TABLE `level` (
  `id_level` int(25) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(25) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","Admin"),
("2","Operator");





CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(25) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(25) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `id_tarif` (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("20190307001","puspita","827ccb0eea8a706c4c34a16891f84e7b","173672"," Puspita Wulandari","","Bogor","197100","1"),
("20190308002","nurul","6968a2c57c3a4fee8fadc79a80355e4d","1453566","Siti Nurul Laela","","Bogor","30000","2");





CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(25) NOT NULL,
  `id_tagihan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(10) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(15) NOT NULL,
  `biaya_denda` int(15) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `id_admin` char(15) NOT NULL,
  PRIMARY KEY (`id_pembayaran`),
  KEY `id_tagihan` (`id_tagihan`,`id_pelanggan`,`id_admin`),
  KEY `id_pelanggan` (`id_pelanggan`),
  KEY `id_admin` (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190403001","20190403001","20190307001","2019-04-03 17:07:36","4","40500","2500","0","43000","erna");





CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190403001","20190307001","4","2019","0","30");





CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190402001","puspita","20000","Bank Mandiri","2019-04-02 17:43:13","TELAH DIVERIFIKASI"),
("20190402002","nurul","10000","Bank Mandiri","2019-04-02 19:03:12","TELAH DIVERIFIKASI"),
("20190403001","puspita","100","Bank BRI","2019-04-03 17:15:17","TELAH DIVERIFIKASI");





CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190403001","20190403001","20190307001","4","2019","30","Lunas");





CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL AUTO_INCREMENT,
  `daya` varchar(25) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","900","1350"),
("2","450","1000"),
("3","1300","2000");



