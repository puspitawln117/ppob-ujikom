<?php  
include 'navbar.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>

<div class="row-fluid sortable">    
        <div class="box span12">
          <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Edit Tarif</h2>
            <div class="box-icon">
              <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
              <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
              <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
          </div>
          <div class="box-content">
                    <form class="form-horizontal" action="" method="POST">
                    <?php
                    $id_tarif=$_GET['id'];
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                    $tarif = mysqli_fetch_array($query_tarif);
                    ?>
              <fieldset>
              <div class="control-group">
                <label class="control-label" for="typeahead">Daya </label>
                <div class="controls">
                <input type="text" class="span6 typeahead" id="typeahead"  name="daya" value="<?php echo $tarif['daya'];?>" class="form-control" required="">
                </div>
              </div> 
              <div class="control-group">
                <label class="control-label" for="typeahead">Tarif Per Kwh </label>
                <div class="controls">
                <input type="text" class="span6 typeahead" id="typeahead"  name="tarifperkwh" value="<?php echo $tarif['tarifperkwh'];?>" class="form-control" required="">
                </div>
              </div> 
              <div class="form-actions">
                <input type="submit" name="edit" value="Edit" class="btn btn-info">
                <a href="d_tarif.php"><span class="btn btn-danger">Batal</span></a>
              </div>
              </fieldset>
            </form>  
          </div>
        </div><!--/span-->
      
      </div><!--/row-->
     <?php
if(isset($_POST['edit'])){
  $daya=$_POST['daya'];
  $tarifperkwh=$_POST['tarifperkwh'];
  
  $edit=mysqli_query($koneksi,"UPDATE tarif SET daya='$daya', tarifperkwh='$tarifperkwh' WHERE id_tarif=$_GET[id]");
  if($edit){
        echo"<script>window.location.assign('d_tarif.php');</script>";
  }else{
    echo"gagal";
  }
}
?>

<?php  
include 'footer.php';
?>