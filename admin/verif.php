<?php  
include 'navbar.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<?php
    $id_saldo = $_GET['id'];
    $pilih = mysqli_query($koneksi, "SELECT * FROM saldo WHERE id_saldo='$id_saldo'");
    $data = mysqli_fetch_array($pilih);
    ?>

<div class="row-fluid sortable">    
        <div class="box span12">
          <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Verifikasi Saldo Pelanggan</h2>
            <div class="box-icon">
              <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
              <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
              <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
          </div>
          <div class="box-content">
                    <form class="form-horizontal" action="" method="POST">
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="typeahead">Username </label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" id="typeahead"  name="username" value="<?php echo $data['username']; ?>" readonly>
							  </div>
							</div> 
							<?php
		                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE username='$data[username]'");
		                      $user = mysqli_fetch_array($query_user);
		                      ?>
                      		<div class="control-group">
							  <label class="control-label" for="typeahead">Nama Pelanggan </label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" id="typeahead"  name="nama_pelanggan" value="<?php echo $user['nama_pelanggan']; ?>" readonly>
							  </div>
							</div> 
							<div class="control-group">
							  <label class="control-label" for="typeahead">Saldo </label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" id="typeahead" name="jumlah_isi" value="<?php echo $data['jumlah_isi']; ?>" readonly>
							  </div>
							</div> 
							<div class="form-actions">
							  <button type="submit" class="btn btn-primary" name="verif">Verifikasi</button>
							  <a href="verifikasi.php"><button type="reset" class="btn">Batal</button></a>
							</div>
						  </fieldset>
						</form>  
          </div>
        </div><!--/span-->
      
      </div><!--/row-->
     <?php
      if (isset($_POST['verif'])){
        $username = $_POST['username'];
        $nama_pelanggan = $_POST['nama_pelanggan'];
        $jumlah_isi = $_POST['jumlah_isi'];
        if ($data['status'] == "TELAH DIVERIFIKASI") {
          echo "<script>window.alert('Status telah diverifikasi')
          window.location='verifikasi.php'</script>";
        } else {
        $saldo = mysqli_query($koneksi,"UPDATE saldo SET username = '$username', jumlah_isi = '$jumlah_isi', status ='TELAH DIVERIFIKASI' WHERE id_saldo='$id_saldo'");
        $pelanggan = mysqli_query($koneksi,"UPDATE pelanggan SET saldo = saldo+'$jumlah_isi' WHERE username='$data[username]'");
        if($saldo AND $pelanggan){
          echo "<script>window.alert('Data Berhasil DiVerifikasi')
          window.location='verifikasi.php'</script>";
        }else{
          echo "Gagal";
        }
      } 
    }
      ?>

<?php  
include 'footer.php';
?>