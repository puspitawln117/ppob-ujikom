<?php  
include 'navbar.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>

<div class="row-fluid sortable">    
        <div class="box span12">
          <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Verifikasi Saldo Pelanggan</h2>
            <div class="box-icon">
              <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
              <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
              <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
          </div>
          <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                <tr>
                    <th>ID Saldo</th>
                    <th>Username</th>
                    <th>Nominal Pengisian</th>
                    <th>Metode Pembayaran</th>
                    <th>Tanggal Pengisian</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
              </thead>   
               <tbody>
                  <?php
                  $query_saldo = mysqli_query($koneksi, "select * from saldo");
                  while($saldo = mysqli_fetch_array($query_saldo)){

                    ?>
                    <tr>
                      <td><?php echo $saldo['id_saldo']; ?></td>
                      <td><?php echo $saldo['username']; ?></td>
                      <td><?php echo $saldo['jumlah_isi']; ?></td>
                      <td><?php echo $saldo['metode']; ?></td>
                      <td><?php echo $saldo['tanggal_pengisian']; ?></td>
                      <td><?php echo $saldo['status']; ?></td>
                      <td>
                        <a href="verif.php?id=<?php echo $saldo['id_saldo']; ?>"><button type="button" class="btn btn-warning"> <span class="glyphicon glyphicon-edit"></span> Verifikasi</button></a>
                        <a href="hapusv.php?id=<?php echo $saldo['id_saldo']; ?>"><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>
                      </td> 
                    </tr>

                  <?php } ?>

                </tbody>
            </table>            
            <button class="btn btn-success" onClick="print_d()">Print Document</button>      
            </div>
        </div><!--/span-->
      
      </div><!--/row-->

<script>
  function print_d(){
   window.open("printv.php","_blank");
}
</script>
<?php  
include 'footer.php';
?>