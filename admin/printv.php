<?php
    include '../konek.php';
    include 'navbar.php';
    $pilih = mysqli_query ($koneksi, "SELECT * FROM saldo");
?>
	<table class="table">
    	 <thead>
            <hr><h3 align="center">Data Verifikasi Saldo</h3>
        <tr>
            <th>ID Saldo</th>
            <th>Username</th>
            <th>Nominal Pengisian</th>
            <th>Metode Pembayaran</th>
            <th>Tanggal Pengisian</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php while($saldo = mysqli_fetch_array($pilih))
    { 
        ?>
            <tr>
                <td><?php echo $saldo['id_saldo']; ?></td>
                <td><?php echo $saldo['username']; ?></td>
                <td><?php echo $saldo['jumlah_isi']; ?></td>
                <td><?php echo $saldo['metode']; ?></td>
                <td><?php echo $saldo['tanggal_pengisian']; ?></td>
                <td><?php echo $saldo['status']; ?></td>
            </tr>
        </tbody>
        <?php } ?>
    </table>
    <script>
        window.load = print_d();
        function print_d(){
            window.print();
        }
    </script>
<?php  
include 'footer.php';
?>