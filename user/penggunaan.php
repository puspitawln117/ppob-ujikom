<?php  
include 'header.php';
require '../Konek.php';
if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>
<div class="container">
<h4 align="center">Data Penggunaan</h4><hr><br>
<table class="table table-responsive bootstrap-datatable datatable" id="example">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Pembayaran</th>
			<th>Bulan Bayar</th>
			<th>Total Bayar</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 
            $no = 1;
			$pilih = mysqli_query ($koneksi,"SELECT * FROM pembayaran where id_pelanggan='$_SESSION[id_pelanggan]'");
			while($data=mysqli_fetch_array($pilih)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['tanggal_pembayaran']; ?></td>
				<td>
					<?php 
						switch ($data['bulan_bayar'])
						{
							case "1";
								$bulan_bayar = "Januari";
							break;
							case "2";
								$bulan_bayar = "Februari";
							break;
							case "3";
								$bulan_bayar = "Maret";
							break;
							case "4";
								$bulan_bayar = "April";
							break;
							case "5";
								$bulan_bayar = "Mei";
							break;
							case "6";
								$bulan_bayar = "Juni";
							break;
							case "7";
								$bulan_bayar = "Juli";
							break;
							case "8";
								$bulan_bayar = "Agustus";
							break;
							case "9";
								$bulan_bayar = "September";
							break;
							case "10";
								$bulan_bayar = "Oktober";
							break;
							case "11";
								$bulan_bayar = "November";
							break;
							case "12";
								$bulan_bayar = "Desember";
							break;
						} 
						echo $bulan_bayar;
					?>
				</td>
				<td><?php echo $data['total_bayar']; ?></td>
				<td>
					<a href="detail.php?id=<?php echo $data['id_pembayaran'];?>" button  class="btn btn-info">Lebih Banyak</button>
				</td>
					</tr>
				<?php  
					}
				?>
			</tbody>
		</table>
        </div>

		<?php  
		include 'footer.php';
		?>