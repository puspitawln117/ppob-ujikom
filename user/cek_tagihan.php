<?php
include 'header.php';
include '../konek.php';
  if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>

 <?php
$today = date("Ymd"); //untuk mengambil tahun, tanggal dan tahun hari ini
$tanggal_daftar = date("d/m/Y");
//cari id terakhir di tanggal hari ini
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];

//setelah membaca id terakhir , lanjut mencari nomor urut id dari id terakhir
$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++; //nomor urut +1

//setelah ketemu id terakhir lanjut membuat id baru dengan format sbb:
$NewID = $today .sprintf('%03s',$NoUrut);
//$today nanti jadinya misal 20160526 .sprintf('%03s', $NoUrut) urutan id di tanggal hari ini

$id_pelanggan = $_SESSION['id_pelanggan'];
$query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' order by id_penggunaan desc");
$penggunaan = mysqli_fetch_array($query_penggunaan);
$id_penggunaan = $penggunaan['id_penggunaan'];

//Penggambilan data di tabel pelanggan
$query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
$pelanggan = mysqli_fetch_array($query_pelanggan);
$nama_pelanggan = $pelanggan['nama_pelanggan'];
$id_tarif = $pelanggan['id_tarif'];

//pengambilan data di tabel tarif berdasarkan id tarif pelanggan
$query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tarif = mysqli_fetch_array($query_tarif);
$tarifperkwh = $tarif['tarifperkwh'];

$bulan_tagihan = $penggunaan['bulan'];
$tahun_tagihan = $penggunaan['tahun'];
$jumlah_meter_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal'];
$jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh;
$tahun_cek = date("Y");
$bulan_cek = date('n');
$tanggal_denda = date('d');
if ($tanggal_denda > 20){
  $jumlah_denda = 5000;
}else {
  $jumlah_denda = 0;
}
$query_tagihan_belum_bayar = mysqli_query($koneksi, "SELECT * FROM tagihan WHERE id_pelanggan='$id_pelanggan' and bulan='$bulan_cek' and tahun='$tahun_cek' and status='Belum Dibayar'");
$cek_tagihan_belum_dibayar = mysqli_num_rows($query_tagihan_belum_bayar);
if ($cek_tagihan_belum_dibayar > 0) {
  echo "<script>window.alert('Jangan cek mulu, masih ada tagihan belum dibayar')</script>";
  $tagihan = mysqli_fetch_array($query_tagihan_belum_bayar);
  $status = $tagihan['status'];
  $id_tagihan = $tagihan['id_tagihan']; 
}
else{
  mysqli_query($koneksi, "INSERT INTO tagihan values ('$NewID','$id_penggunaan','$id_pelanggan','$bulan_tagihan','$tahun_tagihan','$jumlah_meter_penggunaan','Belum Dibayar')");
  $status = "Belum DiBayar";
  $id_tagihan = $NewID;
}
?>

<div class="row">
  <div class="col-md-12">
    <!-- Form Elements -->
    <div class="panel panel-default">
      <div class="panel-heading">
        Detail Tagihan
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <form action="proses_pembayaran.php?p=<?php echo $NewID ?>" method="POST">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nomor KWH</label>
                <div class="col-sm-9">
                  <input type="number" name="nomor_kwh" value="<?php echo $pelanggan['nomor_kwh']; ?>" class="form-control" placeholder="Nomor KWH" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Meter Awal</label>
                <div class="col-sm-9">
                  <input type="number" name="meter_awal" value="<?php echo $penggunaan['meter_awal']; ?>" class="form-control" placeholder="Meter Awal" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Meter Akhir</label>
                <div class="col-sm-9">
                  <input type="number" name="meter_akhir" value="<?php echo $penggunaan['meter_akhir']; ?>" class="form-control" placeholder="Meter Akhir" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Daya (Watt)</label>
                <div class="col-sm-9">
                  <input type="number" name="daya" class="form-control" value="<?php echo $tarif['daya']; ?>" placeholder="No Tagihan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tarif Per-Kwh</label>
                <div class="col-sm-9">
                  <input type="number" name="tarifperkwh" class="form-control" value="<?php echo $tarif['tarifperkwh']; ?>" placeholder="No Pelanggan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Bulan Tagihan</label>
                <div class="col-sm-9">
                  <input type="text" name="bulan" class="form-control" value="<?php echo $penggunaan['bulan']; ?> <?php echo $penggunaan['tahun']; ?>" placeholder="Biaya Admin Bank" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                <div class="col-sm-9">
                  <input type="number" name="jumlah_tagihan" class="form-control" value="<?php echo $jumlah_tagihan; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Biaya Admin</label>
                <div class="col-sm-9">
                  <input type="text" name="biaya_admin" class="form-control" value="2500" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Denda</label>
                <div class="col-sm-9">
                  <input type="text" name="denda" class="form-control" value="<?php echo $jumlah_denda; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Total Bayar</label>
                <div class="col-sm-9">
                  <input type="text" name="total_bayar" class="form-control" value="<?php $jumlah_bayar=$jumlah_tagihan + 2500 + $jumlah_denda; echo $jumlah_bayar; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status</label>
                <div class="col-sm-9">
                  <input type="text" name="status" class="form-control" value="<?php echo $status; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3">
                  <input type="submit" class="btn btn-danger" name="bayar" value="Bayar Sekarang">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
include 'footer.php';
?>