<?php
include 'header.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>

<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">Detail Penggunaan</h4>

         </div>

       </div>
       <!-- /. ROW  -->
       <hr />

       <?php
       $id_penggunaan = $_GET['id'];
       $query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_penggunaan='$id_penggunaan'");
       $penggunaan = mysqli_fetch_array($query_penggunaan);{
       ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Detail
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <form method="POST">
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">ID Penggunaan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['id_penggunaan']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">ID Pelanggan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['id_pelanggan']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Bulan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['bulan']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Tahun</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['tahun']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Meter Awal</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['meter_awal']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Meter Akhir</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" value="<?php echo $penggunaan['meter_akhir']; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                  </form>
                  <button class="btn btn-success" onClick="print_d()">Cetak Struk</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
</div>
</div>

</div>
<script>
  function print_d(){
   window.open("prints.php","_blank");
}
</script>
<?php
include 'footer.php';
?>