<?php
include 'header.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>

<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">Silakan Melakukan Transaksi Untuk Pembayaran</h4>

         </div>

       </div>
       <!-- /. ROW  -->
       <hr />

       <?php
       $query_saldo = mysqli_query($koneksi, "SELECT * FROM saldo WHERE username='$_SESSION[username]' and status='Menunggu Verifikasi'");
       $saldo = mysqli_fetch_array($query_saldo);{
       ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Detail
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <form action="saldo_proses.php" method="POST">
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Username</label>
                      <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" value="<?php echo $saldo['username']; ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Metode Pembayaran</label>
                      <div class="col-sm-9">
                        <input type="text" name="metode" class="form-control" value="<?php echo $saldo['metode']; ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Tanggal Pengisian</label>
                      <div class="col-sm-9">
                        <input type="text" name="tanggal_pengisian" class="form-control" value="<?php echo $saldo['tanggal_pengisian']; ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Nomor Rekening</label>
                      <div class="col-sm-9">
                        <input type="text" name="" class="form-control" value="89562343" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Atas Nama</label>
                      <div class="col-sm-9">
                        <input type="text" name="" class="form-control" value="Puspita Wulandari" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Jumlah yang harus dibayar</label>
                      <div class="col-sm-9">
                        <input type="text" name="jumlah_isi" class="form-control" value="<?php echo $saldo['jumlah_isi']; ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <input type="text" name="status" class="form-control" value="<?php echo $saldo['status']; ?>" readonly>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
</div>
</div>

<?php
include 'footer.php';
?>