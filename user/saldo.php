<?php
include 'header.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>

<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 class="pull-left">Saldo Anda : Rp. <?php echo $admin['saldo']; ?></h4>
         </div>
       </div>
       <!-- /. ROW  -->
       <hr />

       <div class="row">
        <div class="col-md-8">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Form Pengisian Saldo Anda
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <form action="saldo_proses.php" method="POST">
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $_SESSION['username']; ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label>Nominal Isi Saldo</label>
                      <input type="text" name="jumlah_isi" class="form-control" placeholder="Nominal Pengisian Saldo" required>
                    </div>
                    <div class="form-group">
                      <label>Metode Pembayaran</label>
                        <select class="form-control" name="metode">
                          <option value="Bank BRI">Bank BRI</option>
                          <option value="Bank Mandiri">Bank Mandiri</option>
                          <option value="Alfamart">Alfamart</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<?php
include 'footer.php';
?>