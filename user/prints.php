<?php
session_start();
include '../konek.php';
if (empty($_SESSION['username'])) {
    header('location:../index.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../../../favicon.ico">
    <title>M-PLN</title>
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">

</head>
<body>

<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">CETAK STRUK</h4>

         </div>
       </div>
       <!-- /. ROW  -->
       <hr />

        <?php
        $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan");
        $pelanggan = mysqli_fetch_array($query_pelanggan);

        $id_pelanggan = $_SESSION['id_pelanggan'];
        $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran WHERE id_pelanggan='$id_pelanggan'");
        $pembayaran = mysqli_fetch_array($query_pembayaran);{
        ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              STRUK PEMBELIAN
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                    <h3 align="center">STRUK PEMBELIAN LISTRIK PRABAYAR</h3><br>
                </div>
                <div align="center">
                <table>
                  <tr>
                    <td>NOMOR METER</td>
                    <td><?php echo $pelanggan['nomor_kwh']; ?></td>
                    <td>TANGGAL PEMBELIAN</td>
                    <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                  </tr>
                  <tr>
                    <td>ID PELANGGAN</td>
                    <td><?php echo $pelanggan['id_pelanggan']; ?></td>
                    <td>NOMINAL PEMBELIAN</td>
                    <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                  </tr>
                  <tr>
                    <td>NAMA PELANGGAN</td>
                    <td><?php echo $pelanggan['nama_pelanggan']; ?></td>
                    <td>BIAYA ADMIN</td>
                    <td><?php echo $pembayaran['biaya_admin']; ?></td>
                  </tr>
                  <?php
                    $id_tarif = $pelanggan['id_tarif'];
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                    $tarif = mysqli_fetch_array($query_tarif);
                    {
                    ?>
                    <tr>
                    <td>TARIF/DAYA</td>
                    <td><?php echo $tarif['tarifperkwh']; ?>/<?php echo $tarif['daya']; ?></td>
                    <td>BIAYA DENDA</td>
                    <td><?php echo $pembayaran['biaya_denda']; ?></td>
                  </tr>
                  <tr>
                    <td>TOTAL PEMBAYARAN</td>
                    <td><?php echo $pembayaran['total_bayar']; ?></td>
                  </tr>
                  <?php
                    }
                    ?>
                </table>
            </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
</div>
</div>
</div>

<script>
        window.load = print_d();
        function print_d(){
            window.print();
        }
    </script>

  </body>
</html>
<?php
include 'footer.php';
?>