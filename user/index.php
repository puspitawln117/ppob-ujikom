<?php
include 'header.php';
include '../konek.php';
if (empty($_SESSION['username'])) {
  header('location:../index.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
  $pelanggan = mysqli_fetch_array($query_pelanggan);
}
?>

 <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 class="pull-left">Saldo Anda : Rp. <?php echo $pelanggan['saldo']; ?></h4>
           <a href="saldo.php"><button class="btn btn-primary pull-right">Isi Saldo</button></a>
          </div>
        </div>
       <!-- /. ROW  -->
       <hr />

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Cek Tagihan
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <form action="proses_cek_tagihan.php" method="POST">
                    <div class="form-group">
                      <label>Nomor Kwh</label>
                      <input type="number" name="nomor_kwh" class="form-control" placeholder="Enter Nomor Kwh" value="<?php echo $pelanggan['nomor_kwh']; ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label>Meter Akhir</label>
                      <input type="number" name="meter_akhir" class="form-control" placeholder="Enter meter akhir" required>
                      <small><i>*Silahkan masukan meter akhir</i></small>
                    </div>
                    <div class="form-group">
                      <input type="submit" name="cek_tagihan" value="Cek Tagihan" class="btn btn-primary">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

</div>
</div>

<?php
include 'footer.php';
?>